#!C:\Perl\bin\perl.exe
use strict;
use HTML::TreeBuilder;
use HTTP::Request::Common qw(POST);
use LWP::UserAgent;
use List::MoreUtils::PP;
use Config::IniFiles;
use Math::Round qw(nearest);
# set vars
my $week = $ARGV[0] ? $ARGV[0] : 4;
my $mode = $ARGV[1] ? $ARGV[1] : 0;
my $template; 
my %volatility;

my $cfg = Config::IniFiles->new( -file => "config.ini" ) or die($!);
my $pairs = $cfg->{mysects};
open(FIL,"template.set") or die($!);
while(<FIL>){$template .= $_}

#get volatility 
my $url = 'https://www.mataf.net/ru/forex/tools/volatility';  
my $ua = LWP::UserAgent->new;
my $req = POST $url,  ['nbWeeks' => $week ];
my $html = $ua->request($req)->as_string;
my $tree = HTML::TreeBuilder->new();
$tree->parse($html);
$tree->elementify();
my  $table = $tree->look_down('class', 'table table-hover table-condensed clickable');
my @tr = $table-> look_down('class', 'data_volat');
for (@tr){
	my @td = $_->look_down('_tag', 'td');
	$volatility{$td[0]->as_text} = $td[2]->as_text;	
}

#make sets
for(@$pairs){
	my $pair = $_;
	my $PipStarter;
	my $tpl = $template;
	my $MagicNumber_1 = $cfg->val( $pair, 'MagicNumber_1' ); 
	my $MagicNumber_2 = $cfg->val( $pair, 'MagicNumber_2' );
	my $MagicNumber_3 = $cfg->val( $pair, 'MagicNumber_3' );
	my $UseStrategy_1 = $cfg->val( $pair, 'UseStrategy_1' );
	my $UseStrategy_2 = $cfg->val( $pair, 'UseStrategy_2' );
	my $UseStrategy_3 = $cfg->val( $pair, 'UseStrategy_3' );
	my $TakeProfit = $cfg->val( $pair, 'TakeProfit' );
	my $MaxTrades = $cfg->val( $pair, 'MaxTrades' );
	if($mode){
		#		$PipStarter = $volatility{$pair}/$TakeProfit;
		$PipStarter = $TakeProfit;
		$MaxTrades = $volatility{$pair}/$TakeProfit;
		$MaxTrades = nearest(1, $MaxTrades);
	}else{
		$PipStarter = $volatility{$pair}/$MaxTrades;
		$PipStarter += $TakeProfit;
	}	
	$PipStarter = nearest(.00000001, $PipStarter);	
	$tpl =~ s/TakeProfit.*/TakeProfit=$TakeProfit/;
	$tpl =~ s/PipStarter.*/PipStarter=$PipStarter/;
	$tpl =~ s/MaxTrades(_\d).*/MaxTrades$1=$MaxTrades/g;
	$tpl =~ s/MagicNumber_1.*/MagicNumber_1=$MagicNumber_1/;
	$tpl =~ s/MagicNumber_2.*/MagicNumber_2=$MagicNumber_2/;
	$tpl =~ s/MagicNumber_3.*/MagicNumber_3=$MagicNumber_3/;
	$tpl =~ s/UseStrategy_1.*/UseStrategy_1=$UseStrategy_1/;
	$tpl =~ s/UseStrategy_2.*/UseStrategy_2=$UseStrategy_2/;
	$tpl =~ s/UseStrategy_3.*/UseStrategy_3=$UseStrategy_3/;	
	$pair =~ s/\///;    
	open(my $fh, '>', "${pair}.set") or die($!);
	print $fh $tpl;
	close $fh;
}